function createError(message) {
    return `
    <div class="alert alert-danger" role="alert">
      ${message}
    </div>
    `;
  }


function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div>
      <div class="card shadow mb-5 bg-body-tertiary rounded" style="width: 20rem;">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class= "card-subtitle mb-2 text-muted">${location}</p>
          <p class="card-text">${description}</p>
        </div>
        <div class = 'card footer' align="center"> <time>${starts} - ${ends}</time></div>
      </div>
    </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col');
    let colIndex = 0;

    try {
        const response = await fetch(url);

        if (!response.ok) {
            return alert();
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, starts, ends, location);
                    const column = columns[colIndex % 3];
                    column.innerHTML += html;
                    colIndex++;
                }
            }

        }
    } catch (e) {
        console.error(e)
    }
});
